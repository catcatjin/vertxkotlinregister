###
# vert.x docker example using a Java verticle
# To build:
#  docker build -t sample/vertx-java .
# To run:
#   docker run -t -i -p 8080:8080 sample/vertx-java
###

# Extend vert.x image
FROM vertx/vertx3

ENV VERTICLE_FILE target/kotlin-coroutines-register.jar
RUN apt-get update
RUN apt-get install -y maven

# Set the location of the verticles
ENV VERTICLE_HOME /usr/verticles

EXPOSE 8080

# Copy your verticle to the container
# COPY $VERTICLE_FILE $VERTICLE_HOME/
COPY ./pom.xml $VERTICLE_HOME/
ADD src $VERTICLE_HOME/src

# Launch the verticle
WORKDIR $VERTICLE_HOME

RUN ls -alh && mvn clean package
RUN find $HOME/.m2/ -iname "hsqldb*.jar" -exec cp {} $VERTX_HOME/lib/ \;

ENTRYPOINT ["sh", "-c"]
#CMD ["exec vertx run src/main/kotlin/register/App.kt"]
CMD ["exec java -jar $VERTICLE_FILE"]
