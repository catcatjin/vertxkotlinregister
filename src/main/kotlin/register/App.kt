package movierating

import io.vertx.ext.jdbc.JDBCClient
import io.vertx.ext.web.Route
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.kotlin.core.http.listenAwait
import io.vertx.kotlin.core.json.array
import io.vertx.kotlin.core.json.get
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.dispatcher
import io.vertx.kotlin.ext.sql.executeAwait
import io.vertx.kotlin.ext.sql.getConnectionAwait
import io.vertx.kotlin.ext.sql.queryWithParamsAwait
import io.vertx.kotlin.ext.sql.updateWithParamsAwait
import io.vertx.ext.web.handler.RedirectAuthHandler
import io.vertx.ext.web.handler.StaticHandler
import io.vertx.ext.web.handler.CookieHandler
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.ext.web.handler.SessionHandler
import io.vertx.ext.web.sstore.LocalSessionStore
import io.vertx.ext.auth.jdbc.JDBCAuth
import kotlinx.coroutines.launch


class App : CoroutineVerticle() {

  private lateinit var client: JDBCClient
  var usrid = 0

  override suspend fun start() {

    client = JDBCClient.createShared(vertx, json {
      obj(
        "url" to "jdbc:hsqldb:mem:test?shutdown=true",
        "driver_class" to "org.hsqldb.jdbcDriver",
        "max_pool_size-loop" to 30
      )
    })

    // Populate database
    val statements = listOf(
            "CREATE TABLE RATING (ID INTEGER IDENTITY PRIMARY KEY, USER_NAME VARCHAR(16))"
    )
    client.getConnectionAwait()
      .use { connection -> statements.forEach { connection.executeAwait(it) } }

    // Build Vert.x Web router
    val router = Router.router(vertx)
    router.route().handler(CookieHandler.create())
    router.route().handler(BodyHandler.create())
    router.route().handler(SessionHandler.create(LocalSessionStore.create(vertx)))
    router.post("/username/:id").coroutineHandler { ctx -> register(ctx) }

    // Serve the non private static pages
    router.route().handler(StaticHandler.create())

    // Start the server
    vertx.createHttpServer().requestHandler(router).listen(8080)
  }

  //
  suspend fun register(ctx: RoutingContext) {
    var username = ctx.pathParam("id")

    usrid++
    client.getConnectionAwait().use { connection ->
      val isrepead = connection.queryWithParamsAwait("SELECT USER_NAME FROM RATING WHERE USER_NAME LIKE ?", json { array(username + '%' ) })
      if (isrepead.rows.size > 0) {
        username = username + (isrepead.rows.size).toString()
        connection.updateWithParamsAwait("INSERT INTO RATING (ID, USER_NAME) VALUES ?, ?", json { array(usrid, username) })

        val result = client.queryWithParamsAwait("SELECT USER_NAME FROM RATING WHERE ID=?", json { array(usrid) })
        ctx.response().end(json {
          obj("id" to usrid, "username" to result.rows[0]["USER_NAME"]).encode()
        })
      } else {
        connection.updateWithParamsAwait("INSERT INTO RATING (ID, USER_NAME) VALUES ?, ?", json { array(usrid, username) })
        val result = client.queryWithParamsAwait("SELECT USER_NAME FROM RATING WHERE ID=?", json { array(usrid) })
        ctx.response().end(json {
          obj("id" to usrid, "username" to result.rows[0]["USER_NAME"]).encode()
        })
      }
    }
  }
  
  /**
   * An extension method for simplifying coroutines usage with Vert.x Web routers
   */
  fun Route.coroutineHandler(fn: suspend (RoutingContext) -> Unit) {
    handler { ctx ->
      launch(ctx.vertx().dispatcher()) {
        try {
          fn(ctx)
        } catch (e: Exception) {
          ctx.fail(e)
        }
      }
    }
  }
}
