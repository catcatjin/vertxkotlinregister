# Vert.x Kotlin Coroutines Register System

## Build Docker image and save to tar
```
> docker build -t="kvx2" .
> docker save -o vkx.tar kvx2
 
```

## Load Docker
```
> docker load -i ./vkx.tar
> docker run -it -p 8080:8080 kvx2
 
```

## API

The application exposes a REST API for register system:

Finally you can register with user name

```
> curl -X POST http://localhost:8080/username/jacky
{"id":1,"username":"jacky"}
```

## Web Browser

use web browser to view the page of localhost (http://localhost:8080)



